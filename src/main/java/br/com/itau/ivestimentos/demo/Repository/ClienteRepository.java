package br.com.itau.ivestimentos.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.ivestimentos.demo.model.Cliente;


@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
