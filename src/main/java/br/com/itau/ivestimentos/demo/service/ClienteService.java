package br.com.itau.ivestimentos.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.ivestimentos.demo.Repository.ClienteRepository;
import br.com.itau.ivestimentos.demo.model.Cliente;
import br.com.itau.ivestimentos.demo.model.Produto;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienterepository;


	public Iterable<Cliente> obterCliente() {
		System.out.println("Chamaram o listar de clientes :) ");
		return clienterepository.findAll();
	}
	
	public Optional<Cliente> obterCliente(Long id) {
		return clienterepository.findById(id);
	}


	public void criarCliente(Cliente cliente) {
		clienterepository.save(cliente);
		System.out.println("Chamaram a criação do cliente " + cliente.getCpf());
	}
	
	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienterepository.delete(cliente);
		}
	
	
	public Cliente editarCliente(Long id, Cliente clienteAtualizado) {
		Cliente cliente = encontraOuDaErro(id);

		cliente.setNome(clienteAtualizado.getNome());

		return clienterepository.save(cliente);
		}
		
	public Cliente encontraOuDaErro(Long id) {
		Optional<Cliente> optional = clienterepository.findById(id);

		if(!optional.isPresent()) {
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}

		return optional.get();
		}
	
}
