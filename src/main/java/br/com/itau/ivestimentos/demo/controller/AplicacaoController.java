package br.com.itau.ivestimentos.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.ivestimentos.demo.model.Aplicacao;
import br.com.itau.ivestimentos.demo.service.AplicacaoService;
import br.com.itau.ivestimentos.demo.service.ClienteService;

@RestController
public class AplicacaoController {
	@Autowired
	private AplicacaoService aplicacaoService;
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/cliente/{id}/aplicacao")
	public Iterable<Aplicacao> listarAplicacao(@PathVariable Long id) {
		return aplicacaoService.obterAplicacoes(id);
	}
	
	@PostMapping("/cliente/{id}/aplicacao")
	public void salvarAplicacao(@PathVariable Long id, @RequestBody Aplicacao aplicacao) {
		aplicacao.setCliente(clienteService.obterCliente(id).get());
		aplicacaoService.criarAplicacao(aplicacao);
	}
}
