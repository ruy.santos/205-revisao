package br.com.itau.ivestimentos.demo.Repository;

import org.springframework.stereotype.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.ivestimentos.demo.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{

}
