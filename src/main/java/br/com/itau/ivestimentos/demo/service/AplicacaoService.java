package br.com.itau.ivestimentos.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.ivestimentos.demo.Repository.AplicacaoRepository;
import br.com.itau.ivestimentos.demo.model.Aplicacao;

@Service
public class AplicacaoService {
	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	public Iterable<Aplicacao> obterAplicacoes(Long idCliente) {
		return aplicacaoRepository.findAllByCliente_Id(idCliente);
	}
	
	public void criarAplicacao(Aplicacao aplicacao) {
		aplicacaoRepository.save(aplicacao);
	}
}
