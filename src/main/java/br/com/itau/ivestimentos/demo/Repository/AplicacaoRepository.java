package br.com.itau.ivestimentos.demo.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.ivestimentos.demo.model.Aplicacao;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long>{

	Iterable<Aplicacao> findAllByCliente_Id(Long idCliente);

}
