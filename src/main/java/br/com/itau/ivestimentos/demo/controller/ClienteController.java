package br.com.itau.ivestimentos.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.ivestimentos.demo.model.Cliente;
import br.com.itau.ivestimentos.demo.model.Produto;
import br.com.itau.ivestimentos.demo.service.ClienteService;
import br.com.itau.ivestimentos.demo.service.ProdutoService;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/cliente")
	public Iterable<Cliente> listarClientes() {
		return clienteService.obterCliente();
	}

	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarCliente(@RequestBody Cliente cliente) {
		clienteService.criarCliente(cliente);
	}

	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarCliente(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}

	@PatchMapping("/cliente/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
}
