package br.com.itau.ivestimentos.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.ivestimentos.demo.Repository.ProdutoRepository;
import br.com.itau.ivestimentos.demo.model.Produto;
import br.com.itau.ivestimentos.demo.model.Simulacao;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos() {
		return produtoRepository.findAll();
	}

	
	public Produto obterProduto(Long id) {

		return produtoRepository.findById(id).get();
	}
	
	public void criarProduto(Produto produto) {
		produtoRepository.save(produto);
	}
	
	public void apagarProduto(Long id) {
		Produto filme = encontraOuDaErro(id);
		produtoRepository.delete(filme);
	}

	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);
		
		produto.setNome(produtoAtualizado.getNome());
		produto.setRendimento(produtoAtualizado.getRendimento());
		
		return produtoRepository.save(produto);
	}
	
	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Filme não encontrado");
		}
		
		return optional.get();
	}
	
	public Simulacao simularProduto(Simulacao simulacao) {
		Double rendimento = obterProduto(simulacao.getProduto().getId()).getRendimento();
		
		double valorFinal = Math.pow((rendimento + 1), simulacao.getMeses()) * simulacao.getValor();
		simulacao.setResultado(valorFinal);
		return simulacao;
	}
}
